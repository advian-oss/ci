#!/bin/bash -l
set -e
if [ "$#" -eq 0 ]; then
  exec testpublisher -vv docker_config.toml
else
  exec "$@"
fi
