variable "DATE_TAG" {
    default = "undefined"
}

group "default" {
    targets = ["dummywheelhouse"]
}

target "dummywheelhouse" {
    dockerfile = "Dockerfile"
    platforms = ["linux/amd64", "linux/arm64"]
    tags = ["advian/dummywheelhouse:latest", "advian/dummywheelhouse:${DATE_TAG}"]
}
