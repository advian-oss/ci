variable "DATE_TAG" {
    default = "undefined"
}

group "default" {
    targets = ["s3serve"]
}

target "s3serve" {
    dockerfile = "Dockerfile"
    platforms = ["linux/amd64", "linux/arm64"]
    tags = ["advian/s3serve:latest", "advian/s3serve:${DATE_TAG}"]
}
