variable "DATE_TAG" {
    default = "undefined"
}

group "default" {
    targets = ["advian-ci"]
}

target "advian-ci" {
    dockerfile = "Dockerfile"
    platforms = ["linux/amd64", "linux/arm64"]
    args = {
        POETRY_VERSION = "1.2.2"
    }
    tags = ["advian/advian-ci:latest", "advian/advian-ci:${DATE_TAG}"]
}
