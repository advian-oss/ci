# advian-oss/ci

Enable testing and release automation by having a file named `.gitlab-ci.yml` in your
project's root with the following contents:

```yaml
include:
  - project: 'advian-oss/ci'
    file: '/common/.gitlab-ci.yml'
```

## CI image

To build the
[CI Docker image](https://hub.docker.com/r/advian/advian-ci)
that is used to run pipelines, it's necessary to manually start the pipeline for this repo.
The image build process is time-consuming, especially because `arm64` is targeted in addition to `amd64`,
and we need to emulate ARM during the build.

## s3serve image

This pipeline also builds the
[s3serve image](https://hub.docker.com/r/advian/s3serve)
that consists of a
[Caddy](https://caddyserver.com/)
server with
[caddy-s3-proxy plugin](https://github.com/lindenlab/caddy-s3-proxy).
This image can be useful for serving prebuilt Python wheels from an S3 bucket.
Like with the CI image, this build is a slow process because of the ARM emulation involved.

To run a `s3serve` container, it's necessary to supply the following environment variables:

* `REGION`
* `BUCKET`
* `AWS_ACCESS_KEY_ID`
* `AWS_SECRET_ACCESS_KEY`

The credentials can be supplied
[in other ways](https://github.com/lindenlab/caddy-s3-proxy#credentials),
too.
