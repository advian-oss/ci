"""pytest automagics"""
import logging
import urllib.request
import json

import pytest
from libadvian.logging import init_logging

# pylint: disable=W0621
init_logging(logging.DEBUG)
LOGGER = logging.getLogger(__name__)


def echo_is_responsive(url: str) -> bool:
    """Check if we can connect to the db"""
    try:
        LOGGER.debug("trying to urlopen {}".format(url))
        with urllib.request.urlopen(url, timeout=0.5) as fpntr:  # nosec
            data = fpntr.read()
            if not data:
                LOGGER.debug("no data")
                return False
            payload = json.loads(data.decode("utf-8"))
            if not payload:
                LOGGER.debug("empty payload")
                return False
        return True
    except Exception as exc:  # pylint: disable=W0703
        LOGGER.debug("Failed to open, error: {}".format(exc))
        return False


@pytest.fixture(scope="session")
def dockerecho(docker_ip, docker_services) -> str:  # type: ignore
    """start docker container for db"""
    port = docker_services.port_for("echo", 8181)
    url = f"http://{docker_ip}:{port}/"  # pragma: allowlist secret
    docker_services.wait_until_responsive(timeout=10.0, pause=0.5, check=lambda: echo_is_responsive(url))
    return url
