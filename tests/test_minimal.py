"""Minimal tests module"""

import logging
import urllib.request
import json
import os

import ossci

# pylint: disable=W0621
LOGGER = logging.getLogger(__name__)


def test_minimal():  # type: ignore
    """Do a minimal test, succeed"""
    assert ossci.__version__


def test_dockerecho(dockerecho) -> None:  # type: ignore
    """Make sure the pytest-docker service is up"""
    assert dockerecho.startswith("http://")
    with urllib.request.urlopen(dockerecho, timeout=0.5) as fpntr:  # nosec
        data = fpntr.read()
        LOGGER.debug("data={}".format(data))
        payload = json.loads(data.decode("utf-8"))
        assert payload


def test_env_pass() -> None:
    """Test CI passes our test variable"""
    if "CI" not in os.environ:
        return
    assert "POETRY_XX_ENV_TEST" in os.environ
    assert os.environ["POETRY_XX_ENV_TEST"] == "iamhere"
    assert "PIP_XX_ENV_TEST" in os.environ
    assert os.environ["PIP_XX_ENV_TEST"] == "heretoo"
