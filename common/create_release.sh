#!/bin/sh
# Ensure jq is installed
apk add --no-cache jq
# Look for files
if [ -f "${CI_PROJECT_DIR}/pyproject.toml" ]
then
  source /root/.profile
  export VERSION="$(poetry version | cut -d\  -f2)"
elif [ -f "${CI_PROJECT_DIR}/Cargo.toml" ]
then
  export VERSION="$(cargo pkgid | cut -d# -f2 | cut -d: -f2)"
elif [ -f "${CI_PROJECT_DIR}/package.json" ]
then
  export VERSION=$(cat package.json| jq -r '.version')
else
  echo "!!!"
  echo "!!! No usable project info file (pyproject.toml, Cargo.toml, package.json) found"
  echo "!!!"
  exit 1
fi
if [ -z "$VERSION" ]
then
  echo "!!! Got empty version !!!"
  exit 1
fi

set -e
echo "Tagging with $VERSION"
curl -X POST -H "PRIVATE-TOKEN: $API_ACCESS_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/repository/tags?tag_name=$VERSION&message=advian-ci-autotag&ref=$CI_COMMIT_SHA"
echo "Releasing $VERSION"
export GITLAB_PRIVATE_TOKEN=$API_ACCESS_TOKEN
gitlab_auto_release --tag-name $VERSION --release-name $VERSION
