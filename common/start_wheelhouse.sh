#!/bin/sh
tmp=$(docker ps | grep dummywheelhouse)
if [ "$?" == "0" ]
then
  echo "dummywheelhouse is running"
  exit 0
fi
tmp=$(docker ps -a | grep dummywheelhouse)
if [ "$?" == "0" ]
then
  echo "dummywheelhouse is stopped, removing just in case"
  tmp=$(docker rm dummywheelhouse)
fi
echo "Starting dummywheelhouse"
set -e
docker run -d --privileged \
    -p 3141:8000 \
    --name=dummywheelhouse \
    advian/dummywheelhouse:latest
