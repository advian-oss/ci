#!/bin/sh

echo "\nSkipping coverage in $CI_PROJECT_DIR/pytest.ini\n"

cp $CI_PROJECT_DIR/pytest.ini $CI_PROJECT_DIR/pytest.ini.backup

while read -r line; do
  if echo "$line" | grep '^addopts' > /dev/null; then
    parts=$(echo "$line" | cut -d\  -f1-)
    for part in $parts; do
      if ! echo "$part" | grep '^--cov' > /dev/null; then
        echo -n "$part "
      fi
    done
    echo
    continue
  fi
  echo "$line"
done < $CI_PROJECT_DIR/pytest.ini.backup > $CI_PROJECT_DIR/pytest.ini
rm $CI_PROJECT_DIR/pytest.ini.backup
