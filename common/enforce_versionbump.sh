#!/bin/sh

export GIT_BRANCH=$CI_COMMIT_REF_NAME
if [ "${GIT_BRANCH}" != "$CI_DEFAULT_BRANCH" ]; then
  echo "On branch ${GIT_BRANCH}, checking that version has changed."
else
  exit 0
fi

# Python/Poetry
if [ -f "${CI_PROJECT_DIR}/pyproject.toml" ]; then
  echo "pyproject.toml found, enforcing version bump."
  git fetch origin $CI_DEFAULT_BRANCH
  if git diff origin/$CI_DEFAULT_BRANCH -- pyproject.toml | grep '+version'; then
    echo "Version in pyproject.toml differs from $CI_DEFAULT_BRANCH, good."
    exit 0
  else
    echo "!!!"
    echo "!!! Version in pyproject.toml does not differ from $CI_DEFAULT_BRANCH, failing."
    echo "!!!"
    exit 1
  fi
fi

# Rust/Cargo
if [ -f "${CI_PROJECT_DIR}/Cargo.toml" ]; then
  echo "Cargo.toml found, enforcing version bump."
  git fetch origin $CI_DEFAULT_BRANCH
  if git diff origin/$CI_DEFAULT_BRANCH -- Cargo.toml | grep '+version'; then
    echo "Version in Cargo.toml differs from $CI_DEFAULT_BRANCH, good."
    exit 0
  else
    echo "!!!"
    echo "!!! Version in Cargo.toml does not differ from $CI_DEFAULT_BRANCH, failing."
    echo "!!!"
    exit 1
  fi
fi

# Node/React
if [ -f "${CI_PROJECT_DIR}/package.json" ]; then
  echo "package.json found, enforcing version bump."
  git fetch origin $CI_DEFAULT_BRANCH
  if git diff origin/$CI_DEFAULT_BRANCH -- package.json | grep '+\s*"version":'; then
    echo "Version in package.json differs from $CI_DEFAULT_BRANCH, good."
    exit 0
  else
    echo "!!!"
    echo "!!! Version in package.json does not differ from $CI_DEFAULT_BRANCH, failing."
    echo "!!!"
    exit 1
  fi
fi

# Fall-through
echo "No pyproject.toml, package.json or Cargo.toml found, not enforcing version bump."
