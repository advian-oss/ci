#!/bin/sh

rm -rf $CI_PROJECT_DIR/.tox && rm -f $CI_PROJECT_DIR/*test*.xml $CI_PROJECT_DIR/junit*.xml

eval $(ssh-agent -s)
if [ ! -z "${DEPLOY_PRIVATE_KEY}" ]; then
  echo "Adding ${DEPLOY_PRIVATE_KEY}"
  cat $DEPLOY_PRIVATE_KEY | ssh-add -
fi
set -ex
echo '*.env\n*.secrets' >>.gitignore
echo '*.env\n*.secrets' >>.dockerignore
env | grep POETRY >/tmp/poetry.secrets
env | grep PIP >/tmp/pip.secrets
docker --debug build --secret id=poetry,src=/tmp/poetry.secrets --secret id=pip,src=/tmp/pip.secrets --target=${CI_TEST_TARGET-test} --build-arg BUILDKIT_INLINE_CACHE=1 --ssh default --cache-from $TEST_IMAGE_URI --cache-from $CI_REGISTRY_IMAGE:$CI_DEFAULT_BRANCH-${ARCH} --tag $TEST_IMAGE_URI -f $DOCKERFILE . && docker push $TEST_IMAGE_URI
rm /tmp/poetry.secrets /tmp/pip.secrets
env | cut -f1 -d= | grep POETRY >/tmp/run_pass.env
env | cut -f1 -d= | grep PIP >>/tmp/run_pass.env
# Apparently host networking is needed if one wants to run pytest-docker for example
docker run --privileged -v /var/run/docker.sock:/var/run/docker.sock -v /tmp:/tmp -v $CI_PROJECT_DIR:/app -v $SSH_AUTH_SOCK:$SSH_AUTH_SOCK -e SSH_AUTH_SOCK -e CI --env-file /tmp/run_pass.env --network=host $TEST_IMAGE_URI
rm /tmp/run_pass.env
