#!/bin/sh -e

if [ ! -z "${CI_BUILD_TARGETS+x}" ]; then
  eval $(ssh-agent -s)
  if [ ! -z "${DEPLOY_PRIVATE_KEY}" ]; then
    echo "Adding ${DEPLOY_PRIVATE_KEY}"
    cat $DEPLOY_PRIVATE_KEY | ssh-add -
  fi
  env | grep POETRY >/tmp/poetry.secrets
  env | grep PIP >/tmp/pip.secrets
  echo '*.env\n*.secrets' >>.gitignore
  echo '*.env\n*.secrets' >>.dockerignore
  targets="$(echo $CI_BUILD_TARGETS | tr , \  )"
  for target in $targets; do
    echo "Building target ${target}"
    set -x
    docker build \
      --target=$target \
      --build-arg BUILDKIT_INLINE_CACHE=1 \
      --ssh default \
      --secret id=poetry,src=/tmp/poetry.secrets \
      --secret id=pip,src=/tmp/pip.secrets \
      -f $DOCKERFILE .
    set +x
  done;
  rm /tmp/poetry.secrets /tmp/pip.secrets
fi
