#!/bin/sh

export DOCKERFILE_FRONTEND_EXPERIMENTAL="# syntax=docker/dockerfile:1.1.7-experimental"

if [ ! "$(head -1 $DOCKERFILE)" = "$DOCKERFILE_FRONTEND_EXPERIMENTAL" ]; then
  echo "Cramming \"$DOCKERFILE_FRONTEND_EXPERIMENTAL\" into $DOCKERFILE"
  cp $DOCKERFILE $DOCKERFILE.backup
  echo "$DOCKERFILE_FRONTEND_EXPERIMENTAL" > $DOCKERFILE
  cat $DOCKERFILE.backup >> $DOCKERFILE
  rm $DOCKERFILE.backup
fi
