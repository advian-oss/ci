# syntax=docker/dockerfile:1.1.7-experimental
#############################################
# Tox testsuite for multiple python version #
#############################################
FROM advian/tox-base:alpine as tox
# Install system deps and set python versions we need
ARG PYTHON_VERSIONS="3.11 3.10 3.9"
ARG POETRY_VERSION="1.7.1"
RUN --mount=type=secret,id=poetry --mount=type=secret,id=pip true \
    && grep "iamhere" /run/secrets/poetry \
    && grep "heretoo" /run/secrets/pip
RUN export RESOLVED_VERSIONS=`pyenv_resolve $PYTHON_VERSIONS` \
    && echo RESOLVED_VERSIONS=$RESOLVED_VERSIONS \
    && for pyver in $RESOLVED_VERSIONS; do pyenv install -s $pyver; done \
    && pyenv global $RESOLVED_VERSIONS \
    && poetry self update $POETRY_VERSION || pip install -U poetry==$POETRY_VERSION \
    && pip install -U tox \
    && apk add --no-cache \
        zeromq-dev \
        git \
        docker \
        docker-cli-compose \
    && true

######################
# Base builder image #
######################
FROM python:3.11-alpine3.19 as builder_base

ENV \
  # locale
  LC_ALL=C.UTF-8 \
  # python:
  PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  # pip:
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  # poetry:
  POETRY_VERSION=1.7.1


RUN --mount=type=secret,id=poetry --mount=type=secret,id=pip true \
    && grep "iamhere" /run/secrets/poetry \
    && grep "heretoo" /run/secrets/pip
RUN apk add --no-cache \
        curl \
        git \
        bash \
        build-base \
        libffi-dev \
        libxml2-dev \
        libxslt-dev \
        linux-headers \
        openssh-client \
        openssl \
        openssl-dev \
        zeromq \
        tini \
        cargo \
        docker \
        docker-cli-compose \
    # Installing `poetry` package manager:
    && curl -sSL https://install.python-poetry.org | python3 - \
    && echo 'export PATH="/root/.local/bin:$PATH"' >>/root/.profile \
    && export PATH="/root/.local/bin:$PATH" \
    && true

SHELL ["/bin/bash", "-lc"]



# Read GitLab SSH host keys
RUN mkdir -p -m 0700 ~/.ssh && ssh-keyscan gitlab.com > ~/.ssh/known_hosts
# Copy only requirements, to cache them in docker layer:
WORKDIR /pysetup
COPY ./poetry.lock ./pyproject.toml ./README.md /pysetup/
# Install basic requirements (utilizing an internal docker wheelhouse if available)
RUN --mount=type=ssh pip3 install wheel virtualenv poetry-plugin-export \
    && poetry config warnings.export false \
    && poetry export -f requirements.txt --without-hashes -o /tmp/requirements.txt \
    && pip3 wheel --wheel-dir=/tmp/wheelhouse --trusted-host 172.17.0.1 --find-links=http://172.17.0.1:3141 -r /tmp/requirements.txt \
    && virtualenv /.venv && source /.venv/bin/activate && echo 'source /.venv/bin/activate' >>/root/.profile \
    && pip3 install --no-deps --trusted-host 172.17.0.1 --find-links=http://172.17.0.1:3141 --find-links=/tmp/wheelhouse/ /tmp/wheelhouse/*.whl \
    && true


####################################
# Base stage for production builds #
####################################
FROM builder_base as production_build
# Copy entrypoint script
COPY ./docker/entrypoint.sh /docker-entrypoint.sh
# Only files needed by production setup
COPY ./poetry.lock ./pyproject.toml Cargo.toml Cargo.lock ./src /app/
WORKDIR /app
# Build the wheel package with poetry and add it to the wheelhouse
RUN --mount=type=ssh source /.venv/bin/activate \
    && poetry build -f wheel --no-interaction --no-ansi \
    && cargo build --release \
    && cp dist/*.whl /tmp/wheelhouse \
    && chmod a+x /docker-entrypoint.sh \
    && true


#########################
# Main production build #
#########################
FROM python:3.11-alpine3.19 as production
COPY --from=production_build /tmp/wheelhouse /tmp/wheelhouse
COPY --from=production_build /docker-entrypoint.sh /docker-entrypoint.sh
COPY --from=production_build /app/target/release/osscirs /usr/local/bin
WORKDIR /app
# Install system level deps for running the package (not devel versions for building wheels)
# and install the wheels we built in the previous step. generate default config
RUN apk add --no-cache \
        bash \
        libffi \
        zeromq \
        tini \
    && chmod a+x /docker-entrypoint.sh \
    && pip3 install --trusted-host 172.17.0.1 --find-links=http://172.17.0.1:3141 --find-links=/tmp/wheelhouse/ /tmp/wheelhouse/ossci-*.whl \
    && rm -rf /tmp/wheelhouse/ \
    && echo "[dummy]" > docker_config.toml \
    && true
ENTRYPOINT ["/sbin/tini", "--", "/docker-entrypoint.sh"]


#####################################
# Base stage for development builds #
#####################################
FROM builder_base as devel_build
# Install deps
WORKDIR /pysetup
RUN --mount=type=ssh source /.venv/bin/activate \
    && export PIP_FIND_LINKS=http://172.17.0.1:3141 \
    && export PIP_TRUSTED_HOST=172.17.0.1 \
    && poetry install --no-interaction --no-ansi \
    && true


#############
# Run tests #
#############
FROM devel_build as test
COPY . /app
WORKDIR /app
ENTRYPOINT ["/sbin/tini", "--", "docker/entrypoint-test.sh"]
# Re run install to get the service itself installed
RUN --mount=type=ssh source /.venv/bin/activate \
    && export PIP_FIND_LINKS=http://172.17.0.1:3141 \
    && export PIP_TRUSTED_HOST=172.17.0.1 \
    && poetry lock \
    && poetry install --no-interaction --no-ansi \
    && true


###########
# Hacking #
###########
FROM test as devel_shell
ENTRYPOINT ["/bin/bash", "-l"]
